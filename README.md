# README #

A python selenium based web scraper that scrape members profile from 
[m360asia-2017.net-working.com.au](http://m360asia-2017.net-working.com.au/attendee-directory/), and store them in csv format.

### What is this repository for? ###

* To scrape member profiles from [m360asia-2017.net-working.com.au](http://m360asia-2017.net-working.com.au/attendee-directory/). 

### How do I get set up? ###

* Python 2.7
* [requirements.txt](requirements.txt)

### Who do I talk to? ###

* [pallab-gain](https://bitbucket.org/pallab-gain)