import os


def get_driver_path():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(dir_path,"chromedriver")
    return file_path
