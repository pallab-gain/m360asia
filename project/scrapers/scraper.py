from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def get_options():
    chrome_options = Options()
    return chrome_options

def get_browser(chrome_driver_path=None):
    if chrome_driver_path is None:
        return None

    chrome_options = get_options()
    browser = webdriver.Chrome(executable_path=chrome_driver_path, chrome_options=chrome_options)
    return browser
