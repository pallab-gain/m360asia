import csv

from mydriver.get_driver import *
from scrapers.scraper import *
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def doit():
    driver_path = get_driver_path()
    browser = get_browser(chrome_driver_path=driver_path)
    if browser is None:
        raise "browser is none"

    browser.get("http://m360asia-2017.net-working.com.au/attendee-directory/")
    element = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.ID, "wp-submit"))
    )

    #user_login = cgosse@brightcove.com
    #user_pass = 5dfIuTC8

    username = browser.find_element_by_id("user_login")
    password = browser.find_element_by_id("user_pass")

    username.send_keys("cgosse@brightcove.com")
    password.send_keys("5dfIuTC8")
    element.click()

    attendees = WebDriverWait(browser, 15).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "#navbar > ul > li:nth-child(2) > a"))
    )
    attendees.click()

    attendies_list = WebDriverWait(browser, 30).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "body > div.container.networking > div > div > div > h3"))
    )

    # - First    Name
    # - Last    Name
    # - Company    name
    # - Role

    attendees_list = []

    for each_attendee in browser.find_elements_by_css_selector("body > div.container.networking > div > div > div > h3"):
        names = each_attendee.text.split(" ")
        first_name,last_name = names[0],names[1]

        rest_elements = each_attendee.find_element_by_xpath("./following-sibling::p").text.split("\n")
        role = None
        company = None

        try:
            role, company = rest_elements[0], rest_elements[1]
        except Exception:
            company = rest_elements[0]

        # print first_name,';',last_name,';',company,';',role


        payload = {
            "First Name": first_name,
            "Last Name" : last_name,
            "Company name": company,
            "Role": role
        }

        attendees_list.append(payload)

    csv_columns = ["First Name", "Last Name", "Company name", "Role"]
    with open('m360asia-2017.csv', 'wb') as csvfile:  # Just use 'w' mode in 3.x
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()

        for item in attendees_list:
            writer.writerow(item)


    browser.quit()

doit()